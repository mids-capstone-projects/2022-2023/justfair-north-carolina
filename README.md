# JUSTFAIR-North-Carolina (QSIDE)

Welcome to the repository of JUSTFAIR (North Carolina) project!

## Table of Contents
  * [Overview](#overview)
  * [Instructions](#instructions)
  * [Research Questions](#research-questions)
  * [Data & Design](#data---design)
  * [Conclusions](#conclusions)
  * [Authors and acknowledgment](#authors-and-acknowledgment)


## Overview
In line with JUSTFAIR project initiated by [QSIDE](https://qsideinstitute.org/research/criminal-justice/justfair/), this study extends JUSTFAIR to the state of North Carolina. In this project, we investigated different decisions made in criminal courts of North Carolina and evaluated disparities among different demographic groups to demonstrate to what extent personal biases of public officials about race, gender, and socioeconomic factors are producing different decisions outcomes. 

Please follow instructions below to navigate this repository :).

## Instructions
- [**00_Data**](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/tree/main/00_data) lists data used in this study.
- All the dependencies required are listed in [`requirements.txt`](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/requirements.txt). 
- [**10_Code**](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/tree/main/10_Code) contains all the code for the analysis, which covers three main parts of the study (notebooks for producing intermediate tables and visuals can also be found here): 
    - [system admission analysis](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/10_Code/admissions_analysis.ipynb)
    - [prosecution analysis](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/10_Code/prosecution_delta_severity_analysis.ipynb)
    - [sentencing analysis](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/10_Code/sentencing_analysis.ipynb) 
    
- [**20_Report**](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/tree/main/20_Report) contains the final [research paper](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/20_Report/QSIDES_Final_Paper.pdf) and a condensed version of [report](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/20_Report/QSIDES_Capstone_Report.pdf) targeted for non-technical readers.
- [**30_Decks**](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/tree/main/30_Decks) contains [capstone symposium slide decks](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/30_Decks/March2023_presentation.pdf).


## Research Questions
Through the lens of this study, we are interested in answering the following research inquiries:
- How can racial bias manifest in the judicial  process of our local district courts? 
- What metrics can we use to measure racial bias in data as the closest reflection of the phenomenon?
- How does decision-making vary across independent district courts in North Carolina? 

## Data & Design
The primary dataset used for the analysis was obtained from the Automated Criminal & Infractions System (ACIS), which contains case information as recorded by all criminal court clerks’ offices in North Carolina. The information available in the dataset dictates the types of analyses that can be made with enough robustness to draw conclusions about bias as a plausible cause of disparities in decision outcomes. Therefore, this study focuses on major procedures in the judicial process:  **Prosecution**,  **Sentencing**, and supplemented with **System Admission** to conclude the analysis.


## Conclusions
Our findings support some claims that are seen often in this field’s literature:

A clear racial disparity exists in the rate of involvement in criminal cases, and this disparity varies between counties. However, we did not observe statistically significant racial and gender disparities in pre-trial activities, sentencing, and plea-bargaining leniency at the aggregate state level for Black defendants. Disparities in plea bargaining affecting Hispanic defendants may be explained by limitations of the metrics capturing severity, and further research is needed to fully understand the disadvantages suffered by Hispanic defendants in plea deals. While there are many potential explanations for this phenomenon, systemic racism remains a plausible factor.

Moreover, our study reveals significant disparities in the severity of decisions made in cases involving minority groups in certain counties when compared to the statewide average. The substantial variation in the behavior of district courts in different counties highlights the differences in the justice systems that can exist within a single state. Notably, counties with disproportionally severe decisions regarding bail bonds, plea bargaining, and sentencing of minority groups, when compared to White defendants, are also more likely to demonstrate higher levels of overrepresentation of minorities, particularly Black populations, as cases.

In conclusion, this study provides evidence supporting claims of racial biases in decisions made in the criminal justice courts in North Carolina. Further research is needed to fully understand the mechanisms behind these disparities and to identify potential solutions. However, our findings highlight the importance of addressing systemic racism and the need for accountability mechanism to ensure a fair and just criminal justice system for all individuals, regardless of demographic or socio-economic factors. 

Detailed information and reports can be found below:
- If you are a non-technical person and would like to explore our findings, please see our condensed and brief version of report [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/20_Report/QSIDES_Capstone_Report.pdf).
- If you are more interested in the technical details as well as a comprehensive background, please see our research paper [here](https://gitlab.oit.duke.edu/mids-capstone-projects/2022-2023/justfair-north-carolina/-/blob/main/20_Report/QSIDES_Final_Paper.pdf).

## Authors and acknowledgment
**Authors**

Preet Khowaja

[Dorothy Hou](mailto:houtianyun0611@gmail.com)

Clarissa Aché​

Chuhan Zhou​

**Acknowledgement**

Many thanks to Ryan Huang and Greg Herschlag, who led the Capstone course, our mentor Nick Eubank, and Jude Higdon from QSIDE for their support, guidance and valuable feedback throughout this project.


